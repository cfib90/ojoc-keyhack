#!/bin/bash
source=$(curl -s "https://apk-dl.com/com.tellm.android.app")

version=$(echo $source | grep -Po "(?<=\"softwareVersion\" : \").+?(?=\")")
date=$(echo $source | grep -Po "(?<=\"datePublished\" : \").+?(?=\")")
apkurl=$(echo $source | grep -Po "http:\/\/apkfind.com\/root\/apk\/[0-9]+\/[0-9]+\/[0-9]+\/com.tellm.android.app_[0-9]+\.apk")

filename="com.tellm.android.app-$version.apk"
echo "current version is $version, published on $date" >&2
wget -O $filename $apkurl"?dl=2"
echo "$filename"
