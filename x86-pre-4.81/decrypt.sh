#!/bin/bash

#
# Copyright (c) 2017 Christian Fibich
#

# This script aims to automate the process of decrypting the key
# It first downloads the most recent APK
# Then unzips the libhmac.so for x86
# And finally tries to decrypt the key using objdump disassembly

SCRIPT_DIR=$(dirname $0)

echo -e "\x1b[1m####   Downloading APK    ####\x1b[0m" >&2

apkfile=$("$SCRIPT_DIR/download_apk.sh")

if [ "$?" != "0" ]; then
    echo "Download failed" >&2
    exit -1
fi

echo -e "\x1b[1m#### Unzipping libhmac.so ####\x1b[0m" >&2

unzip -o "$apkfile" lib/x86/libhmac.so >&2

if [ "$?" != "0" ]; then
    echo "Unzip failed" >&2
    exit -1
fi

echo -e "\x1b[1m####   Decrypting key     ####\x1b[0m" >&2


echo -ne "\x1b[1m\x1b[7m" >&2
"$SCRIPT_DIR/libhmac-objdump-decrypt.sh" lib/x86/libhmac.so
echo -ne "\x1b[0m" >&2

if [ "$?" != "0" ]; then
    echo "Decrypt failed" >&2
    exit -1
fi

