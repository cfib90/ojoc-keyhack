#!/bin/bash

# Copyright (c) 2017 Christian Fibich
#
# This script tries to automatically find the HMAC key.
# It uses the fact that the encrypted key is intialized in the
# "Java_com_jodelapp_jodelandroidv3_api_HmacInterceptor_init" function
#
# FIXME This script will break if this function does anything other
# (using movb and movl instructions) than initializing the key...
#
# Currently we grep for instructions looking like this:
#
# movb   $0x92,0x1a0(%ebx)
# movl   $0xa894e8d0,0x19c(%ebx)
#
# FIXME Tested with 4.48.1, 4.56.1, 4.57.1

OBJDUMP=x86_64-linux-gnu-objdump
SCRIPT_DIR=$(dirname $0)

libhmacso=$1

if [ "x$libhmacso" == "x" ]; then
echo "Usage $0 /path/to/libhmac.so" 1>&2
exit -1
fi

asmtemp=$(mktemp)
c=$(mktemp)
exe=$(mktemp)

"$OBJDUMP" -d "$libhmacso" | sed -r -n '/<Java_com_jodelapp_jodelandroidv3_api_HmacInterceptor_init(@@Base)?>:/,/^[0-9a-f]+ .*:$/p' | grep -o 'mov[bl].*(%e[ab]x)' > "$asmtemp"

(
echo """#include <stdint.h>
#include <stdio.h>
#include \"decrypt.h\"

int main (void) {
   uint8_t buf[100];
"""

offset=$(sed -r 's/mov[bl]\s+\$(0x[0-9a-f]+),(0x[0-9a-f]+).*/\2/' "$asmtemp" | sort | head -1)

echo "int offset = $offset;"
sed -r 's/movb\s+\$(0x[0-9a-f]+),(0x[0-9a-f]+).*/*(char *)(buf + \2 - offset) = \1;/' "$asmtemp" | \
sed -r 's/movl\s+\$(0x[0-9a-f]+),(0x[0-9a-f]+).*/*(int32_t *)(buf + \2 - offset) = \1;/'

echo """    return decrypt(buf);
}"""
) > "$c"

cc -I"$SCRIPT_DIR" -std=c11 -x c "$c" "$SCRIPT_DIR/decrypt.c" -o $exe && $exe && rm $exe

rm $c
rm $asmtemp

