#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>

#include "decrypt.h"


int main(void) {
    uint8_t arr[CLIENT_SECRET_SIZE];

    int i = 0;
    unsigned int x;

    while(i < CLIENT_SECRET_SIZE && scanf("%x",&x) == 1) { 
        arr[i++] = x;
    }

    assert(i == 40);

    decrypt(arr);

    return 0;
}
