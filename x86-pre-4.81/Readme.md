## x86 HMAC Key Retrieval ##

NOTE: This only works up to app version 4.81 and exists in this repo just for
      documentation purposes.

Jodel hides the API key in the `libhmac.so` in the Android app.

The tools in this directory try to retrieve the key from this library.

### Getting Started ###

A Linux environment with `wget`, `gcc`, `objdump` for x86 and `unzip` is assumed.

1. The script `decrypt.sh` aims to automize the key retrieval as far as possible.

   It downloads the APK, unzips the `libhmac.so` for x86 and tries to decrypt
   the key.

   It is executed without parameters.

2. Add the HMAC key to `OJOC/Config.py` in the directory where you cloned
   OJOC using the correct version strings for the app.

        APP_VERSION  = '4.42.4'

        [...]

        APP_CONFIG={
            [...]
            '4.42.4': ConfigType('asdFASFdadadsadASDAasdasASDASDASassdFSdF', version_string='4.42.4', x_api_version='0.2')
        }

    _Of course, substitute `4.42.4` with the correct app version you downloaded the APK for_

