#/!bin/bash
set -eu

# ------------------------------------------------------------------------------

# This function performs the actual decryption process
# It requires one argument: the target ELF (.so) file
# It prints its output to stdout, so the correct usage is
# rv=$(decrypt /path/to/so)
function decrypt {
    FUNCTION_PATTERN="HmacInterceptor_init"
    RADARE2=r2
    OBJDUMP=objdump
    SCRIPT_DIR=$(dirname $0)
    libfile=$1

    bintemp=$(mktemp)
    asmtemp=$(mktemp)
    c=$(mktemp)
    exe=$(mktemp)

    echo "wtf $bintemp \`is~$FUNCTION_PATTERN[5]\` @ \`is~$FUNCTION_PATTERN[1]\`" | "$RADARE2" -q $libfile &> /dev/null

    if [ $? != 0 ] || [ ! -s $bintemp ]; then
        echo "<No key decrypted (Could not create binary dump)>"
        return 255
    fi

    # disassemble binary file to $asmtemp
    "$OBJDUMP" -b binary -D $bintemp  -m i386 | grep -o mov[bl].* > $asmtemp

    if [ ! -s $asmtemp ]; then
        echo "<No key decrypted (Could not create disassembly)>"
        return 255
    fi

    # make C file from sorted lines in $asmtemp
    (
    echo """#include <stdint.h>
    #include <stdio.h>
    #include \"decrypt.h\"

    int main (void) {
       uint8_t buf[100];
    """

    offset=$(sed -r 's/mov[bl]\s+\$(0x[0-9a-f]+),(0x[0-9a-f]+).*/\2/' "$asmtemp" | sort | head -1)

    echo "int offset = $offset;"
    sed -r 's/movb\s+\$(0x[0-9a-f]+),(0x[0-9a-f]+).*/*(char *)(buf + \2 - offset) = \1;/' "$asmtemp" | \
    sed -r 's/movl\s+\$(0x[0-9a-f]+),(0x[0-9a-f]+).*/*(int32_t *)(buf + \2 - offset) = \1;/'

    echo """    return decrypt(buf);
    }"""
    ) > "$c"

    cc -I"$SCRIPT_DIR" -std=c11 -x c "$c" "$SCRIPT_DIR/decrypt.c" -o $exe && $exe && rm $exe

    rm $bintemp $asmtemp $c
    return 0
}

# ------------------------------------------------------------------------------
# This is the "main" part of the script.

ARGC=$#

if [ "$ARGC" == "0" ] || [ "x$1" == "x-h" ]; then
    echo "Usage: $0 [-h] | file1 file2 ... fileN" >&2
    exit 255
fi

while (( "$#" )); do

    libfile=$1

    if ! [ -e "$libfile" ]; then
        echo "File '$libfile' does not exist." >&2
        exit 255
    fi

    echo "$libfile:" $(decrypt $libfile)

    shift

done


