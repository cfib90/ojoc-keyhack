## x86 HMAC Key Retrieval ##

NOTE: This works for app versions >4.81

Jodel hides the API key in the a shared library (`*.so`) supplied with the
the Android app.

The tools in this directory try to retrieve the key from the x86 version of this library.

### Getting Started ###

A Linux environment with `perl`, `gcc`, `objdump` for x86 and `unzip` is assumed.

Additionally, the reverse-engineering framework `radare2` is needed.

The script `decrypt-liba-readelf` tries to decrypt the key in the supplied file(s)

Usage (Single file):

    $ ./decrypt-liba-readelf [...APK extract path...]/lib/x86/liba.so

Usage (Multiple files):

    $ ./decrypt-liba-readelf [...APK extract path...]/lib/x86/liba.so [...APK extract path...]/lib/x86/libd.so

Example:
    
    $ ./decrypt-liba-readelf.sh /tmp/jodel/lib/x86/*.so
    /tmp/jodel/lib/x86/libd.so: zes[...]dZH
    /tmp/jodel/lib/x86/libe.so: <No key decrypted (Function 'HmacInterceptor_init' not found)>
    /tmp/jodel/lib/x86/libimagepipeline.so: <No key decrypted (Function 'HmacInterceptor_init' not found)>

